//
//  ImageCell.swift
//  lab9v2
//
//  Created by Dmitry on 29/01/2020.
//  Copyright © 2020 Dmitry. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet weak var imgurImage: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
}
