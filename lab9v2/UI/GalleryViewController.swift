//
//  GalleryViewController.swift
//  lab9v2
//
//  Created by Dmitry on 29/01/2020.
//  Copyright © 2020 Dmitry. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

private let reuseId = "Cell"

class GalleryViewController: UICollectionViewController {
    let noImageUrl = "https://www.nocowboys.co.nz/images/v3/no-image-available.png"
    
    var allImageUrl: [String] = []
    var elements = [GalleryApiResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let refreshControl = UIRefreshControl()
        self.collectionView.refreshControl = refreshControl
        refreshControl.addTarget(
            self,
            action: #selector(refreshData(_:)),
            for: .valueChanged
        )


    }
    
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
    // MARK: - UICollectionViewFlowLayout
            
            if let flowLayout = self.collectionView.collectionViewLayout as?
                UICollectionViewFlowLayout {
                
                let itemsPerLine: CGFloat = 3
                let itemSpacing: CGFloat = 5
                let lineSpacing: CGFloat = 5
                let padding: CGFloat = 5
                
                let adjustedWidth =
                    self.collectionView.bounds.size.width
                        + itemSpacing
                        - padding * 2
                
                let itemWidth: CGFloat = adjustedWidth / itemsPerLine - itemSpacing
                
                let itemHeight: CGFloat = itemWidth
                
                flowLayout.itemSize = CGSize(width: itemWidth, height: itemHeight)
                flowLayout.minimumInteritemSpacing = itemSpacing
                flowLayout.minimumLineSpacing = lineSpacing
                
                flowLayout.sectionInset =
                    UIEdgeInsets(top: padding,
                                 left: padding,
                                 bottom: padding,
                                 right: padding)
                

                self.collectionView.reloadData()
                
            }
        }

        // MARK: - RequestingData
        func refreshGallery(_ complition: @escaping () -> Void) {
            
            ImgurClient.shared.requestGalleries { (data, error) in
                
                DispatchQueue.main.async {
                    
                    complition()
                    
                    
                    if let allData = data{
                        
                        for elem in allData {
                            
                            let dataResponse = GalleryApiResponse.init(JSON: elem)
                            
                            if let img = dataResponse?.image,
                            img.count > 0{
                                
                                self.allImageUrl.append(img[0].link!)

                            }else {
                                self.allImageUrl.append(self.noImageUrl)
                            }
                            
//                            print(dataResponse?.id)
                            
                            
//                            print(dataResponse?.data[0].image?[0].link)
                            
                        }
                        self.collectionView.reloadData()
                    }
                    
                }
                    
            }
        }
    
    // MARK: - Actions
    @objc
    func refreshData(_ sender: UIRefreshControl) -> Void {
        self.refreshGallery {
            sender.endRefreshing()
        }
        
    }
    
    
    func loadMode() {
        print("Boo")
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionFooter) {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CartFooterCollectionReusableView", for: indexPath)
            
            
            
            return footerView
        }
        fatalError()
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        
        loadMode()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items

        return allImageUrl.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as? ImageCell else { fatalError() }
        
        cell.spinner.startAnimating()
        
        let imageUrl = allImageUrl[indexPath.item]
        
        Alamofire.request(imageUrl).responseImage { (response) in
            
            if let image = response.result.value {
                cell.imgurImage.image = image
                
                cell.spinner.stopAnimating()

                cell.imgurImage.af_cancelImageRequest()
                cell.imgurImage.layer.removeAllAnimations()
                
                
//                DispatchQueue.main.async {
//                    print(response)
//                }
            }
            
        }
        
        // Configure the cell
        cell.backgroundColor = .white
        
//        func prepareForReuse(){
//          cell.spinner.stopAnimating()
//            cell.imgurImage.af_cancelImageRequest()
//            cell.imgurImage.layer.removeAllAnimations()
//
//        }
        
    
        return cell
    }


    
    
    // MARK: UICollectionViewDelegate


}
