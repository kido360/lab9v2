//
//  AuthManager.swift
//  lab9v2
//
//  Created by Dmitry on 29/01/2020.
//  Copyright © 2020 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

class AuthManager: RequestAdapter {
    let imgurId = "cc60e651a676fed"
    let imgurSecret = "801305d4115295e46a4c149c4601f7792ae2cc3d"
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        
        var adaptedRequest = urlRequest
        adaptedRequest.setValue("Client-ID \(self.imgurId)", forHTTPHeaderField: "Authorization")
        
        return adaptedRequest
    }

}
