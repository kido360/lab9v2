//
//  GalleryResponse.swift
//  lab9v2
//
//  Created by Dmitry on 29/01/2020.
//  Copyright © 2020 Dmitry. All rights reserved.
//

import Foundation
import ObjectMapper

class GalleryApiResponse: Mappable {
    
//    var data: [Data]
//
//    required init?(map: Map) {
//        data = []
//    }
//    init(){
//        data = []
//    }
//
//    func mapping(map: Map) {
//        data <- map["data"]
//    }
    var id: String?
    var image: [Image]
    
    required init?(map: Map) {
        image = []
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        image <- map["images"]
    }
    
}

//class Data: Mappable {
//    var id: String?
//    var image: [Image]?
//
//    required init?(map: Map) {
//        image = []
//    }
//
//    func mapping(map: Map) {
//        id <- map["id"]
//        image <- map["images"]
//    }
//    
//}

class Image: Mappable {
    
    var link: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        link <- map["link"]
    }
    
}


