//
//  ImgurClient.swift
//  lab9v2
//
//  Created by Dmitry on 29/01/2020.
//  Copyright © 2020 Dmitry. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class ImgurClient: SessionManager {
    
      static let shared = ImgurClient()
      
      override init(
          configuration: URLSessionConfiguration = URLSessionConfiguration.default,
          delegate: SessionDelegate = SessionDelegate(),
          serverTrustPolicyManager: ServerTrustPolicyManager? = nil
      ) {
          
          super.init(
              configuration: configuration,
              delegate: delegate,
              serverTrustPolicyManager: serverTrustPolicyManager
          )
          
          self.adapter = AuthManager()
          
      }
    
//      @discardableResult
//      func requestGalleries(_ complition: @escaping (String?, Error?) -> Void
//      ) -> DataRequest {
//
//          return self.request("https://api.imgur.com/3/gallery/hot/viral/week")
//              .validate()
//              .responseString (completionHandler: { (dataResponse) in
//
//                  if let err = dataResponse.error {
//                      complition(nil, err)
//                  } else if let response = dataResponse.value {
//
//                          complition(response, nil)
//
//                  } else {
//                      fatalError("Err 001")
//                  }
//          })
//      }
    
    //    ---------------ORIGINAL [[]]-----------------
        
        @discardableResult
        func requestGalleries(_ complition: @escaping ([[String:Any]]?, Error?) -> Void
        ) -> DataRequest {
    
            return self.request("https://api.imgur.com/3/gallery/hot/viral/week")
                .validate()
                .responseJSON (completionHandler: { (dataResponse) in
    
                    if let err = dataResponse.error {
                        complition(nil, err)
                    } else if let response = dataResponse.value as? [String: Any] {
    
                        if let galeries = response["data"] as? [[String: Any]] {
                            complition(galeries, nil)
                        }else {
                            fatalError("Err 002")
                        }
                    } else {
                        fatalError("Err 001")
                    }
            })
        }

    
}
